import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {environment} from '../../environments/environment';
import {GenerateTheme} from '../components/viewModels/generator/generateTheme';

@Injectable({
  providedIn: 'root'
})
export class GeneratorService {
  constructor(private httpService: HttpService) {
  }

  public  generateTheme(vm: GenerateTheme): Promise<any> {
    return this.httpService.getHttp().post(environment.endPoint + '/generator/add', JSON.stringify(vm), this.httpService.getOptions()).toPromise();
  }
}
