import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient) {
  }

  public getOptions(): any {
    return this.httpOptions;
  }

  public  getHttp(): HttpClient {
    return this.http;
  }

  public getData(): Observable<any> {
    console.log(environment.endPoint + '/data/all/');
    return this.http.get(environment.endPoint + '/data/all/', this.httpOptions);
  }

  public addProduct(product): Observable<any> {
    console.log(product);
    return this.http.post<any>(environment.endPoint + '/products', JSON.stringify(product), this.httpOptions);
  }
}
