import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from '../components/app/app.component';
import {AppRoutingModule} from '../routings/approuting.module';
import {HomeComponent} from '../components/home/home.component';
import {GeneratorThemeComponent} from '../components/generators/theme/generatortheme.component';
import {FormsModule} from '@angular/forms';
import {GeneratorVideoComponent} from '../components/generators/video/generatortheme.component';
import {HttpClientModule} from '@angular/common/http';
import {HttpService} from '../services/http.service';
import {GeneratorService} from '../services/generator.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GeneratorThemeComponent,
    GeneratorVideoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    HttpService,
    GeneratorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
