import { NgModule } from '@angular/core';
import {GeneratorRoutingModule} from '../routings/generatorrouting.module';
import {CommonModule} from '@angular/common';


@NgModule({
  imports: [
    CommonModule,
    GeneratorRoutingModule
  ],
  providers: []
})
export class GeneratorModule { }
