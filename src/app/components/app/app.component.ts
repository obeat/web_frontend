import { Component } from '@angular/core';
import {HttpService} from '../../services/http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'front';

  constructor(private httpService: HttpService) {
    this.httpService.getData().toPromise().then(
      onfulfilled => {
        console.log(onfulfilled);
      }
    )
  }
}
