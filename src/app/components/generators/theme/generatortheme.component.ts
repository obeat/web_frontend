import {Component, OnInit} from '@angular/core';
import {GeneratorService} from '../../../services/generator.service';
import {GenerateTheme} from '../../viewModels/generator/generateTheme';

@Component({
  selector: 'app-generator-theme',
  templateUrl: './generatortheme.component.html',
  styleUrls: ['./generatortheme.component.css']
})
export class GeneratorThemeComponent implements OnInit {

  public durationUsed: boolean = false;
  public duration: number = 1.0;
  public durationTxt: string = '';

  public bpmUsed: boolean = false;
  public bpm: number = 100;
  public bpmTxt: string = '';

  constructor(private generatorService: GeneratorService) {}

  ngOnInit(): void {
  }

  public updateChangeDuration() {
    if (this.durationUsed) {
      this.durationTxt = this.duration + '';
    } else {
      this.durationTxt = '';
    }
  }

  public updateChangeBpm() {
    if (this.bpmUsed) {
      this.bpmTxt = this.bpm + '';
    } else {
      this.bpmTxt = '';
    }
  }

  public generateTheme() {
    console.log('Generating music...')
    const vm = new GenerateTheme();
    vm.bpm = this.bpm;
    vm.theme = 0;
    vm.time = this.duration;
    this.generatorService.generateTheme(vm).then(
      onResult => {
        console.log(onResult);
      }
    );
  }
}
