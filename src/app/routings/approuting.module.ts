import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {GeneratorModule} from '../modules/generator.module';
import {PageNotFoundComponent} from '../components/pageNotFound/pagenotfound.component';

const appRoutes: Routes = [
  /*{
    path: 'generator',
    loadChildren: () => GeneratorModule
  },
  { path: '', redirectTo: '/generator', pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent}*/
];

@NgModule({
  declarations: [
    PageNotFoundComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }
