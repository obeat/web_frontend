import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {GeneratorThemeComponent} from '../components/generators/theme/generatortheme.component';
import {FormsModule} from '@angular/forms';

const appRoutes: Routes = [
  {path: '', component: GeneratorThemeComponent},
  {path: 'theme', component: GeneratorThemeComponent}
];

@NgModule({
  declarations: [
    // GeneratorThemeComponent
  ],
  imports: [
    RouterModule.forChild(appRoutes),
    FormsModule
  ],
  exports: [
    RouterModule
  ]
})

export class GeneratorRoutingModule { }
